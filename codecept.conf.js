exports.config = {
  output: './output',
  helpers: {
    Appium: {
      platform: 'Android',
      device: 'Galaxy',
      automationName: 'Appium',
      desiredCapabilities: {
        appPackage: 'com.leapxpert.manager.qa',
        appActivity: 'com.leapxpertapp.MainActivity',
        noReset: false,
        fullReset: false,
        automationName: 'UIAutomator2',
        newCommandTimeout: 30000
      }
    }
  },
  include: {
    I: './steps_file.js',
    LoginMobilePage: './pages/login_mobile.js',
    LoginMobileStep: './steps/login_mobile.js',
    commonAction: './steps/common.js'  
  },
  mocha: {
    reporterOptions: {
      mochaFile: 'output/result.xml',
      reportDir: 'output/result.html'
    }
  },
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: ['./step_definitions/steps.js']
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    },
    retryFailedStep: {
      enabled: true
    }
  },
  rerun: {
    minSuccess: 1,
    maxReruns: 6
  },
 // tests: './test/*',
  name: 'lxp-app-automation'
}